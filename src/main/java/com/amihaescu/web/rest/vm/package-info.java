/**
 * View Models used by Spring MVC REST controllers.
 */
package com.amihaescu.web.rest.vm;
